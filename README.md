The Main file contains the copied task code with numbered lines, for a more understandable display of information in the diagram.

Stack Heap Diagram.pdf - the diagram itself, where row is the line number of the program, and scope - scope of stack variables

Because the stack is built on the principle of first in last out, then the scheme was built in the same way. Therefore, it should be read from the bottom up.
